package com.l.calculadora;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText usuario;
    private EditText contra_et;
    private Button ingresar_boton;
    private Button salir_boton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        usuario = findViewById(R.id.usuario_et);
        contra_et = findViewById(R.id.contra_et);
        ingresar_boton = findViewById(R.id.ingresar_boton);
        salir_boton = findViewById(R.id.salir_boton);


        ingresar_boton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (usuario.getText().toString().isEmpty() || contra_et.getText().toString().isEmpty()){
                    Toast.makeText(MainActivity.this,"Llene el campo para continuar",Toast.LENGTH_SHORT).show();
                }else if(usuario.getText().toString().equals(getString(R.string.user)) && contra_et.getText().toString().equals(getString(R.string.pass))){

                    Intent intent = new Intent(MainActivity.this,CalculadoraActivity.class);
                    startActivity(intent);

                }else {
                    Toast.makeText(MainActivity.this,"Usuario o contraseña incorrecta",Toast.LENGTH_SHORT).show();
                }

            }
        });

        salir_boton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

}
