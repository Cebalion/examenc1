package com.l.calculadora;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class CalculadoraActivity extends AppCompatActivity {

    private TextView usuario;
    private TextView resultado;
    private EditText num1;
    private EditText num2;
    private Button sumar;
    private Button restar;
    private Button multiplicar;
    private Button dividir;
    private Button limpiar;
    private Button salir_boton;
    Calculadora calculadora = new Calculadora();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculadora);

        usuario = findViewById(R.id.usuario);
        usuario.setText(R.string.nombre);

        num1= findViewById(R.id.num1);
        num2= findViewById(R.id.num2);
        resultado = findViewById(R.id.resultado_et);
        sumar = findViewById(R.id.suma_boton);
        restar= findViewById(R.id.resta_boton);
        multiplicar= findViewById(R.id.multiplicacion_boton);
        dividir= findViewById(R.id.division_boton);
        limpiar= findViewById(R.id.limpiar_boton);
        salir_boton= findViewById(R.id.regresar_boton);


        sumar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (vacio()){
                    calculadora.setNum1(Float.parseFloat(num1.getText().toString()));
                    calculadora.setNum2(Float.parseFloat(num2.getText().toString()));
                    resultado.setText("Resultado: "+ calculadora.suma());
                }else {
                    Toast.makeText(CalculadoraActivity.this, "Campo vacio", Toast.LENGTH_SHORT).show();
                }
            }
        });

        restar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (vacio()){
                    calculadora.setNum1(Float.parseFloat(num1.getText().toString()));
                    calculadora.setNum2(Float.parseFloat(num2.getText().toString()));
                    resultado.setText("Resultado: "+ calculadora.resta());
                }else {
                    Toast.makeText(CalculadoraActivity.this, "Campo vacio", Toast.LENGTH_SHORT).show();
                }
            }
        });

        multiplicar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (vacio()){
                    calculadora.setNum1(Float.parseFloat(num1.getText().toString()));
                    calculadora.setNum2(Float.parseFloat(num2.getText().toString()));
                    resultado.setText("Resultado: "+ calculadora.multiplicacion());
                }else {
                    Toast.makeText(CalculadoraActivity.this, "Campo vacio", Toast.LENGTH_SHORT).show();
                }
            }
        });

        dividir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (vacio()){
                    calculadora.setNum1(Float.parseFloat(num1.getText().toString()));
                    calculadora.setNum2(Float.parseFloat(num2.getText().toString()));
                    resultado.setText("Resultado: "+ calculadora.division());
                }else {
                    Toast.makeText(CalculadoraActivity.this, "Campo vacio", Toast.LENGTH_SHORT).show();
                }
            }
        });

        limpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                num1.setText("");
                num2.setText("");
            }
        });
        salir_boton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    public boolean vacio(){
        if (num1.getText().toString().isEmpty() || num2.getText().toString().isEmpty()){
            return false;
        } else return true;
    }
}
